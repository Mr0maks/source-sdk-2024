set(GAMENAME cstrike)

include(${SRCDIR}/game/server/server_base.cmake)

project_include_directories( ${SRCDIR}/game/shared/cstrike cstrike cstrike/bot cstrike/bot/states  ${SRCDIR}/game/shared/cstrike/bot cstrike/hostage)
project_compile_definitions(BOTS CSTRIKE_DLL)

if (WINDOWS)
	project_link_libraries(ws2_32)
endif()

remove_project_sources(AI_ConCommands.cpp)

project_pch_sources(cbase.h
	hl2/Func_Monitor.cpp
	h_cycler.cpp
	${SRCDIR}/game/shared/predicted_viewmodel.cpp
	team_objectiveresource.cpp
	team_spawnpoint.cpp
	${SRCDIR}/game/shared/teamplayroundbased_gamerules.cpp

	${SRCDIR}/game/shared/cstrike/achievements_cs.cpp
	${SRCDIR}/game/shared/cstrike/basecsgrenade_projectile.cpp
	${SRCDIR}/game/shared/cstrike/cs_ammodef.cpp
	cstrike/cs_autobuy.cpp
	${SRCDIR}/game/shared/cstrike/cs_blackmarket.cpp
	cstrike/cs_client.cpp
	cstrike/cs_eventlog.cpp
	cstrike/cs_gameinterface.cpp
	${SRCDIR}/game/shared/cstrike/cs_gamemovement.cpp
	${SRCDIR}/game/shared/cstrike/cs_gamerules.cpp
	cstrike/cs_gamestats.cpp
	${SRCDIR}/game/shared/cstrike/cs_gamestats_shared.cpp
	cstrike/cs_hltvdirector.cpp
	cstrike/cs_player.cpp
	cstrike/cs_player_resource.cpp
	${SRCDIR}/game/shared/cstrike/cs_player_shared.cpp
	${SRCDIR}/game/shared/cstrike/cs_playeranimstate.cpp
	cstrike/cs_playermove.cpp
	${SRCDIR}/game/shared/cstrike/cs_shareddefs.cpp
	cstrike/cs_team.cpp
	${SRCDIR}/game/shared/cstrike/cs_usermessages.cpp
	cstrike/cs_vehicle_jeep.cpp
	${SRCDIR}/game/shared/cstrike/cs_weapon_parse.cpp
	${SRCDIR}/game/shared/cstrike/flashbang_projectile.cpp
	cstrike/func_bomb_target.cpp
	cstrike/func_buy_zone.cpp
	cstrike/func_hostage_rescue.cpp
	cstrike/funfact_cs.cpp
	cstrike/funfactmgr_cs.cpp
	${SRCDIR}/game/shared/cstrike/fx_cs_shared.cpp
	${SRCDIR}/game/shared/cstrike/hegrenade_projectile.cpp
	cstrike/info_view_parameters.cpp
	cstrike/item_ammo.cpp
	cstrike/item_assaultsuit.cpp
	cstrike/item_defuser.cpp
	cstrike/item_kevlar.cpp
	cstrike/item_nvgs.cpp
	cstrike/mapinfo.cpp
	cstrike/point_surroundtest.cpp
	cstrike/smokegrenade_projectile.cpp
	cstrike/te_radioicon.cpp
	cstrike/te_shotgun_shot.cpp
	cstrike/holiday_gift.cpp

	${SRCDIR}/game/shared/cstrike/weapon_ak47.cpp
	${SRCDIR}/game/shared/cstrike/weapon_aug.cpp
	${SRCDIR}/game/shared/cstrike/weapon_awp.cpp
	${SRCDIR}/game/shared/cstrike/weapon_basecsgrenade.cpp
	${SRCDIR}/game/shared/cstrike/weapon_c4.cpp
	${SRCDIR}/game/shared/cstrike/weapon_csbase.cpp
	${SRCDIR}/game/shared/cstrike/weapon_csbasegun.cpp
	${SRCDIR}/game/shared/cstrike/weapon_deagle.cpp
	${SRCDIR}/game/shared/cstrike/weapon_elite.cpp
	${SRCDIR}/game/shared/cstrike/weapon_famas.cpp
	${SRCDIR}/game/shared/cstrike/weapon_fiveseven.cpp
	${SRCDIR}/game/shared/cstrike/weapon_flashbang.cpp
	${SRCDIR}/game/shared/cstrike/weapon_g3sg1.cpp
	${SRCDIR}/game/shared/cstrike/weapon_galil.cpp
	${SRCDIR}/game/shared/cstrike/weapon_glock.cpp
	${SRCDIR}/game/shared/cstrike/weapon_hegrenade.cpp
	${SRCDIR}/game/shared/cstrike/weapon_knife.cpp
	${SRCDIR}/game/shared/cstrike/weapon_m249.cpp
	${SRCDIR}/game/shared/cstrike/weapon_m3.cpp
	${SRCDIR}/game/shared/cstrike/weapon_m4a1.cpp
	${SRCDIR}/game/shared/cstrike/weapon_mac10.cpp
	${SRCDIR}/game/shared/cstrike/weapon_mp5navy.cpp
	${SRCDIR}/game/shared/cstrike/weapon_p228.cpp
	${SRCDIR}/game/shared/cstrike/weapon_p90.cpp
	${SRCDIR}/game/shared/cstrike/weapon_scout.cpp
	${SRCDIR}/game/shared/cstrike/weapon_sg550.cpp
	${SRCDIR}/game/shared/cstrike/weapon_sg552.cpp
	${SRCDIR}/game/shared/cstrike/weapon_smokegrenade.cpp
	${SRCDIR}/game/shared/cstrike/weapon_tmp.cpp
	${SRCDIR}/game/shared/cstrike/weapon_ump45.cpp
	${SRCDIR}/game/shared/cstrike/weapon_usp.cpp
	${SRCDIR}/game/shared/cstrike/weapon_xm1014.cpp

	cstrike/bot/cs_bot.cpp
	cstrike/bot/cs_bot_chatter.cpp
	cstrike/bot/cs_bot_event.cpp
	cstrike/bot/cs_bot_event_bomb.cpp
	cstrike/bot/cs_bot_event_player.cpp
	cstrike/bot/cs_bot_event_weapon.cpp
	cstrike/bot/cs_bot_init.cpp
	cstrike/bot/cs_bot_listen.cpp
	cstrike/bot/cs_bot_manager.cpp
	cstrike/bot/cs_bot_nav.cpp
	cstrike/bot/cs_bot_pathfind.cpp
	cstrike/bot/cs_bot_radio.cpp
	cstrike/bot/cs_bot_statemachine.cpp
	cstrike/bot/cs_bot_update.cpp
	cstrike/bot/cs_bot_vision.cpp
	cstrike/bot/cs_bot_weapon.cpp
	cstrike/bot/cs_bot_weapon_id.cpp
	cstrike/bot/cs_gamestate.cpp

	cstrike/bot/states/cs_bot_attack.cpp
	cstrike/bot/states/cs_bot_buy.cpp
	cstrike/bot/states/cs_bot_defuse_bomb.cpp
	cstrike/bot/states/cs_bot_escape_from_bomb.cpp
	cstrike/bot/states/cs_bot_fetch_bomb.cpp
	cstrike/bot/states/cs_bot_follow.cpp
	cstrike/bot/states/cs_bot_hide.cpp
	cstrike/bot/states/cs_bot_hunt.cpp
	cstrike/bot/states/cs_bot_idle.cpp
	cstrike/bot/states/cs_bot_investigate_noise.cpp
	cstrike/bot/states/cs_bot_move_to.cpp
	cstrike/bot/states/cs_bot_open_door.cpp
	cstrike/bot/states/cs_bot_plant_bomb.cpp
	cstrike/bot/states/cs_bot_use_entity.cpp

	${SRCDIR}/game/shared/cstrike/bot/bot.cpp
	${SRCDIR}/game/shared/cstrike/bot/bot_hide.cpp
	${SRCDIR}/game/shared/cstrike/bot/bot_manager.cpp
	${SRCDIR}/game/shared/cstrike/bot/bot_profile.cpp
	${SRCDIR}/game/shared/cstrike/bot/bot_util.cpp
	${SRCDIR}/game/shared/cstrike/bot/shared_util.cpp

	cstrike/hostage/cs_simple_hostage.cpp

	cstrike/cs_nav_area.cpp
	cstrike/cs_nav_generate.cpp
	cstrike/cs_nav_mesh.cpp
	cstrike/cs_nav_path.cpp)

project_sources(${SRCDIR}/game/shared/cstrike/cs_urlretrieveprices.cpp)
