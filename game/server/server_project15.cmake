set(GAMENAME project15)

include(${SRCDIR}/game/server/server_base.cmake)

project_include_directories( ${SRCDIR}/game/shared/project15 project15 project15/bot project15/bot/states  ${SRCDIR}/game/shared/project15/bot project15/hostage)
project_compile_definitions(BOTS CSTRIKE_DLL)

if (WINDOWS)
	project_link_libraries(ws2_32)
endif()

remove_project_sources(AI_ConCommands.cpp)

project_pch_sources(cbase.h
	hl2/Func_Monitor.cpp
	h_cycler.cpp
	${SRCDIR}/game/shared/predicted_viewmodel.cpp
	team_objectiveresource.cpp
	team_spawnpoint.cpp
	${SRCDIR}/game/shared/teamplayroundbased_gamerules.cpp

	${SRCDIR}/game/shared/project15/achievements_cs.cpp
	${SRCDIR}/game/shared/project15/basecsgrenade_projectile.cpp
	${SRCDIR}/game/shared/project15/cs_ammodef.cpp
	project15/cs_autobuy.cpp
	${SRCDIR}/game/shared/project15/cs_blackmarket.cpp
	project15/cs_client.cpp
	project15/cs_eventlog.cpp
	project15/cs_gameinterface.cpp
	${SRCDIR}/game/shared/project15/cs_gamemovement.cpp
	${SRCDIR}/game/shared/project15/cs_gamerules.cpp
	project15/cs_gamestats.cpp
	${SRCDIR}/game/shared/project15/cs_gamestats_shared.cpp
	project15/cs_hltvdirector.cpp
	project15/cs_player.cpp
	project15/cs_player_resource.cpp
	${SRCDIR}/game/shared/project15/cs_player_shared.cpp
	${SRCDIR}/game/shared/project15/cs_playeranimstate.cpp
	project15/cs_playermove.cpp
	${SRCDIR}/game/shared/project15/cs_shareddefs.cpp
	project15/cs_team.cpp
	${SRCDIR}/game/shared/project15/cs_usermessages.cpp
	project15/cs_vehicle_jeep.cpp
	${SRCDIR}/game/shared/project15/cs_weapon_parse.cpp
	${SRCDIR}/game/shared/project15/flashbang_projectile.cpp
	project15/func_bomb_target.cpp
	project15/func_buy_zone.cpp
	project15/func_hostage_rescue.cpp
	project15/funfact_cs.cpp
	project15/funfactmgr_cs.cpp
	${SRCDIR}/game/shared/project15/fx_cs_shared.cpp
	${SRCDIR}/game/shared/project15/hegrenade_projectile.cpp
	project15/info_view_parameters.cpp
	project15/item_ammo.cpp
	project15/item_assaultsuit.cpp
	project15/item_defuser.cpp
	project15/item_kevlar.cpp
	project15/item_nvgs.cpp
	project15/mapinfo.cpp
	project15/point_surroundtest.cpp
	project15/smokegrenade_projectile.cpp
	project15/te_radioicon.cpp
	project15/te_shotgun_shot.cpp
	project15/holiday_gift.cpp

	${SRCDIR}/game/shared/project15/weapon_ak47.cpp
	${SRCDIR}/game/shared/project15/weapon_aug.cpp
	${SRCDIR}/game/shared/project15/weapon_awp.cpp
	${SRCDIR}/game/shared/project15/weapon_basecsgrenade.cpp
	${SRCDIR}/game/shared/project15/weapon_c4.cpp
	${SRCDIR}/game/shared/project15/weapon_csbase.cpp
	${SRCDIR}/game/shared/project15/weapon_csbasegun.cpp
	${SRCDIR}/game/shared/project15/weapon_deagle.cpp
	${SRCDIR}/game/shared/project15/weapon_elite.cpp
	${SRCDIR}/game/shared/project15/weapon_famas.cpp
	${SRCDIR}/game/shared/project15/weapon_fiveseven.cpp
	${SRCDIR}/game/shared/project15/weapon_flashbang.cpp
	${SRCDIR}/game/shared/project15/weapon_g3sg1.cpp
	${SRCDIR}/game/shared/project15/weapon_galil.cpp
	${SRCDIR}/game/shared/project15/weapon_glock.cpp
	${SRCDIR}/game/shared/project15/weapon_hegrenade.cpp
	${SRCDIR}/game/shared/project15/weapon_knife.cpp
	${SRCDIR}/game/shared/project15/weapon_m249.cpp
	${SRCDIR}/game/shared/project15/weapon_m3.cpp
	${SRCDIR}/game/shared/project15/weapon_m4a1.cpp
	${SRCDIR}/game/shared/project15/weapon_mac10.cpp
	${SRCDIR}/game/shared/project15/weapon_mp5navy.cpp
	${SRCDIR}/game/shared/project15/weapon_p228.cpp
	${SRCDIR}/game/shared/project15/weapon_p90.cpp
	${SRCDIR}/game/shared/project15/weapon_scout.cpp
	${SRCDIR}/game/shared/project15/weapon_sg550.cpp
	${SRCDIR}/game/shared/project15/weapon_sg552.cpp
	${SRCDIR}/game/shared/project15/weapon_smokegrenade.cpp
	${SRCDIR}/game/shared/project15/weapon_tmp.cpp
	${SRCDIR}/game/shared/project15/weapon_ump45.cpp
	${SRCDIR}/game/shared/project15/weapon_usp.cpp
	${SRCDIR}/game/shared/project15/weapon_xm1014.cpp

	project15/bot/cs_bot.cpp
	project15/bot/cs_bot_chatter.cpp
	project15/bot/cs_bot_event.cpp
	project15/bot/cs_bot_event_bomb.cpp
	project15/bot/cs_bot_event_player.cpp
	project15/bot/cs_bot_event_weapon.cpp
	project15/bot/cs_bot_init.cpp
	project15/bot/cs_bot_listen.cpp
	project15/bot/cs_bot_manager.cpp
	project15/bot/cs_bot_nav.cpp
	project15/bot/cs_bot_pathfind.cpp
	project15/bot/cs_bot_radio.cpp
	project15/bot/cs_bot_statemachine.cpp
	project15/bot/cs_bot_update.cpp
	project15/bot/cs_bot_vision.cpp
	project15/bot/cs_bot_weapon.cpp
	project15/bot/cs_bot_weapon_id.cpp
	project15/bot/cs_gamestate.cpp

	project15/bot/states/cs_bot_attack.cpp
	project15/bot/states/cs_bot_buy.cpp
	project15/bot/states/cs_bot_defuse_bomb.cpp
	project15/bot/states/cs_bot_escape_from_bomb.cpp
	project15/bot/states/cs_bot_fetch_bomb.cpp
	project15/bot/states/cs_bot_follow.cpp
	project15/bot/states/cs_bot_hide.cpp
	project15/bot/states/cs_bot_hunt.cpp
	project15/bot/states/cs_bot_idle.cpp
	project15/bot/states/cs_bot_investigate_noise.cpp
	project15/bot/states/cs_bot_move_to.cpp
	project15/bot/states/cs_bot_open_door.cpp
	project15/bot/states/cs_bot_plant_bomb.cpp
	project15/bot/states/cs_bot_use_entity.cpp

	${SRCDIR}/game/shared/project15/bot/bot.cpp
	${SRCDIR}/game/shared/project15/bot/bot_hide.cpp
	${SRCDIR}/game/shared/project15/bot/bot_manager.cpp
	${SRCDIR}/game/shared/project15/bot/bot_profile.cpp
	${SRCDIR}/game/shared/project15/bot/bot_util.cpp
	${SRCDIR}/game/shared/project15/bot/shared_util.cpp

	project15/hostage/cs_simple_hostage.cpp

	project15/cs_nav_area.cpp
	project15/cs_nav_generate.cpp
	project15/cs_nav_mesh.cpp
	project15/cs_nav_path.cpp

	# Project15
	project15/effects/chicken.cpp
	project15/effects/chicken.h
)

project_sources(${SRCDIR}/game/shared/project15/cs_urlretrieveprices.cpp)
