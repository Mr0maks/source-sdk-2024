project(client_${GAMENAME})

set(OUTBINDIR ${OUTDIR}/${GAMENAME}/bin/${PLATSUBDIR})

include(${SRCDIR}/cmake/source_dll_base.cmake)
include(${SRCDIR}/cmake/nut_builder.cmake)

add_nut(${SRCDIR}/game/client/vscript_client.nut)

set_target_properties(client_${GAMENAME} PROPERTIES OUTPUT_NAME client)

project_include_directories(. ${SRCDIR}/game/shared game_controls ${CMAKE_CURRENT_BINARY_DIR})
project_compile_definitions(NO_STRING_T CLIENT_DLL VECTOR VERSION_SAFE_STEAM_API_INTERFACES PROTECTED_THINGS_ENABLE)

if (NOT POSIX)
	project_compile_definitions(sprintf=use_Q_snprintf_instead_of_sprintf strncpy=use_Q_strncpy_instead _snprintf=use_Q_snprintf_instead)
endif()

if (WINDOWS)
	project_compile_definitions(fopen=dont_use_fopen)
endif()

project_compile_definitions(ENABLE_CHROMEHTMLWINDOW ENABLE_STUDIO_SOFTBODY)

if (WINDOWS)
	project_link_libraries(ws2_32)
endif()

add_pch(cbase.h ${SRCDIR}/game/client/stdafx.cpp)

project_pch_sources(cbase.h
	hl2/C_Func_Monitor.cpp
	geiger.cpp
	history_resource.cpp # NOT CSGO
	hud_weapon.cpp
	train.cpp
	${SRCDIR}/game/shared/weapon_parse_default.cpp
	${SRCDIR}/game/shared/achievement_saverestore.cpp
	${SRCDIR}/game/shared/achievementmgr.cpp

	achievement_notification_panel.cpp # NOT CSGO
	${SRCDIR}/game/shared/activitylist.cpp
	${SRCDIR}/game/shared/ammodef.cpp
	animatedentitytextureproxy.cpp
	animatedoffsettextureproxy.cpp
	animatedtextureproxy.cpp
	AnimateSpecificTextureProxy.cpp
	${SRCDIR}/game/shared/animation.cpp
	${SRCDIR}/game/shared/base_playeranimstate.cpp
	${SRCDIR}/game/shared/baseachievement.cpp
	baseanimatedtextureproxy.cpp
	baseclientrendertargets.cpp
	${SRCDIR}/game/shared/basecombatcharacter_shared.cpp
	${SRCDIR}/game/shared/basecombatweapon_shared.cpp
	${SRCDIR}/game/shared/baseentity_shared.cpp
	${SRCDIR}/game/shared/basegrenade_shared.cpp
	${SRCDIR}/game/shared/baseparticleentity.cpp
	${SRCDIR}/game/shared/baseplayer_shared.cpp
	${SRCDIR}/game/shared/baseviewmodel_shared.cpp
	beamdraw.cpp
	${SRCDIR}/game/shared/beam_shared.cpp
	${SRCDIR}/common/blackbox_helper.cpp
	${SRCDIR}/public/bone_accessor.cpp
	${SRCDIR}/game/shared/bone_merge_cache.cpp
	c_ai_basehumanoid.cpp
	c_ai_basenpc.cpp
	c_baseanimating.cpp
	c_baseanimatingoverlay.cpp
	c_basecombatcharacter.cpp
	c_basecombatweapon.cpp
	c_basedoor.cpp
	c_baseentity.cpp
	c_baseflex.cpp
	c_baselesson.cpp
	c_baseplayer.cpp
	c_basetoggle.cpp
	c_baseviewmodel.cpp
	c_beamspotlight.cpp
	c_breakableprop.cpp
	c_colorcorrection.cpp
	c_colorcorrectionvolume.cpp
	c_dynamiclight.cpp
	c_entitydissolve.cpp
	c_entityfreezing.cpp
	c_entityparticletrail.cpp
	c_env_ambient_light.cpp
	c_env_cascade_light.cpp
	csm_parallel_split.cpp
	c_env_fog_controller.cpp
	c_env_particlescript.cpp
	c_env_projectedtexture.cpp
	c_env_screenoverlay.cpp
	c_env_tonemap_controller.cpp
	c_env_dof_controller.cpp
	c_entityflame.cpp
	c_fire_smoke.cpp
	c_fish.cpp
	c_func_areaportalwindow.cpp
	c_func_breakablesurf.cpp
	c_func_brush.cpp
	c_func_conveyor.cpp
	c_func_dust.cpp
	c_func_lod.cpp
	c_func_movelinear.cpp
	c_func_occluder.cpp
	c_func_reflective_glass.cpp
	c_func_rotating.cpp
	c_func_smokevolume.cpp
	c_func_tracktrain.cpp
	c_gameinstructor.cpp
	c_gib.cpp
	c_hairball.cpp
	c_info_overlay_accessor.cpp

	c_lightglow.cpp
	C_MaterialModifyControl.cpp
	c_mod_lesson_stubs.cpp
	c_movie_display.cpp
	c_particle_system.cpp
	c_physbox.cpp
	c_physics_prop_statue.cpp
	c_physicsprop.cpp
	c_physmagnet.cpp
	c_pixel_visibility.cpp
	c_plasma.cpp
	c_playerresource.cpp
	c_point_camera.cpp
	c_point_commentary_node.cpp
	c_postprocesscontroller.cpp
	c_prop_hallucination.cpp
	c_props.cpp
	c_ragdoll_manager.cpp
	c_rope.cpp
	c_rumble.cpp
	c_sceneentity.cpp
	c_shadowcontrol.cpp
	c_slideshow_display.cpp
	c_soundscape.cpp
	c_spatialentity.cpp
	c_spotlight_end.cpp
	c_sprite.cpp
	c_sprite_perfmonitor.cpp
	c_sun.cpp
	c_sunlightshadowcontrol.cpp
	c_surfacerender.cpp
	c_team.cpp
	c_tesla.cpp
	c_test_proxytoggle.cpp
	c_triggers.cpp
	c_user_message_register.cpp
	c_vehicle_choreo_generic.cpp
	c_vehicle_jeep.cpp
	c_vguiscreen.cpp
	hl2/c_waterbullet.cpp

	C_WaterLODControl.cpp
	c_world.cpp

	camomaterialproxy.cpp
	cdll_client_int.cpp
	cdll_bounded_cvars.cpp
	cdll_util.cpp
	cl_mat_stub.cpp
	classmap.cpp
	clientalphaproperty.cpp
	client_factorylist.cpp
	client_thinklist.cpp
	cliententitylist.cpp
	clientleafsystem.cpp
	clientmode_shared.cpp
	clientshadowmgr.cpp
	clientsideeffects.cpp
	clientsideeffects_test.cpp

	colorcorrectionmgr.cpp
	commentary_modelviewer.cpp
	${SRCDIR}/game/shared/collisionproperty.cpp
	convarproxy.cpp
	cycleproxy.cpp
	${SRCDIR}/game/shared/death_pose.cpp
	${SRCDIR}/game/shared/debugoverlay_shared.cpp
	${SRCDIR}/game/shared/decals.cpp
	detailobjectsystem.cpp
	dummyproxy.cpp
	${SRCDIR}/game/shared/effect_dispatch_data.cpp
	EffectsClient.cpp
	${SRCDIR}/game/shared/ehandle.cpp
	${SRCDIR}/game/shared/entitylist_base.cpp
	entityoriginmaterialproxy.cpp
	${SRCDIR}/game/shared/EntityParticleTrail_Shared.cpp
	${SRCDIR}/game/shared/env_detail_controller.cpp
	${SRCDIR}/game/shared/env_wind_shared.cpp
	${SRCDIR}/game/shared/eventlist.cpp
	flashlighteffect.cpp

	foundryhelpers_client.cpp

	${SRCDIR}/game/shared/func_ladder.cpp
	functionproxy.cpp
	fx_blood.cpp
	fx_cube.cpp
	fx_explosion.cpp
	fx_fleck.cpp
	fx_impact.cpp
	fx_interpvalue.cpp
	fx_quad.cpp
	fx_shelleject.cpp
	fx_staticline.cpp
	fx_tracer.cpp
	fx_trail.cpp
	fx_water.cpp
	${SRCDIR}/game/shared/game_timescale_shared.cpp
	${SRCDIR}/game/shared/gamemovement.cpp
	${SRCDIR}/game/shared/gamerules.cpp
	${SRCDIR}/game/shared/gamerules_register.cpp
	${SRCDIR}/game/shared/GameStats.cpp
	${SRCDIR}/game/shared/gamestringpool.cpp
	gametrace_client.cpp
	${SRCDIR}/game/shared/gamevars_shared.cpp
	glow_outline_effect.cpp
	glow_overlay.cpp
	${SRCDIR}/game/shared/hintmessage.cpp
	${SRCDIR}/game/shared/hintsystem.cpp
	hltvcamera.cpp
	hud.cpp
	hud_animationinfo.cpp
	hud_basechat.cpp
	hud_basetimer.cpp
	hud_bitmapnumericdisplay.cpp
	hud_closecaption.cpp
	hud_crosshair.cpp
	hud_element_helper.cpp
	hud_hintdisplay.cpp
	hud_locator_target.cpp
	hud_msg.cpp
	hud_numericdisplay.cpp
	hud_pdump.cpp
	hud_redraw.cpp
	hud_vehicle.cpp

	${SRCDIR}/game/shared/igamesystem.cpp
	in_camera.cpp
	in_joystick.cpp
	in_main.cpp
	initializer.cpp
	${SRCDIR}/public/tier1/interpolatedvar.cpp
	IsNPCProxy.cpp
	lampbeamproxy.cpp
	lamphaloproxy.cpp
	${SRCDIR}/game/shared/mapentities_shared.cpp
	materialproxydict.cpp
	mathproxy.cpp
	matrixproxy.cpp
	menu.cpp
	message.cpp
	modelrendersystem.cpp
	movehelper_client.cpp
	${SRCDIR}/game/shared/movevars_shared.cpp
	vgui_movie_display.cpp
	${SRCDIR}/game/shared/multiplay_gamerules.cpp
	object_motion_blur_effect.cpp
	${SRCDIR}/game/shared/obstacle_pushaway.cpp
	panelmetaclassmgr.cpp
	particle_collision.cpp
	particle_litsmokeemitter.cpp
	${SRCDIR}/game/shared/particle_parse.cpp
	${SRCDIR}/game/shared/particle_property.cpp
	particle_proxies.cpp
	particle_simple3d.cpp
	particlemgr.cpp
	particles_attractor.cpp
	particles_ez.cpp
	particles_localspace.cpp
	particles_new.cpp
	particles_simple.cpp
	${SRCDIR}/game/shared/particlesystemquery.cpp
	perfvisualbenchmark.cpp
	physics.cpp

	physics_main_client.cpp
	${SRCDIR}/game/shared/physics_main_shared.cpp
	${SRCDIR}/game/shared/physics_saverestore.cpp
	${SRCDIR}/game/shared/physics_shared.cpp
	physpropclientside.cpp
	playerandobjectenumerator.cpp
	${SRCDIR}/game/shared/point_bonusmaps_accessor.cpp
	${SRCDIR}/game/shared/point_posecontroller.cpp
	${SRCDIR}/game/shared/precache_register.cpp
	${SRCDIR}/game/shared/predictableid.cpp
	prediction.cpp
	${SRCDIR}/game/shared/predictioncopy.cpp
	${SRCDIR}/game/shared/predictioncopy_helpers.cpp
	${SRCDIR}/game/shared/predictioncopy_test.cpp
	${SRCDIR}/game/shared/props_shared.cpp
	proxyentity.cpp
	ProxyHealth.cpp
	proxyplayer.cpp
	proxypupil.cpp
	ragdoll.cpp
	${SRCDIR}/game/shared/ragdoll_shared.cpp
	recvproxy.cpp
	replaycamera.cpp
	${SRCDIR}/game/shared/rope_helpers.cpp
	${SRCDIR}/game/shared/saverestore.cpp
	${SRCDIR}/game/shared/sceneentity_shared.cpp
	ScreenSpaceEffects.cpp
	${SRCDIR}/game/shared/sequence_Transitioner.cpp
	simple_keys.cpp
	${SRCDIR}/game/shared/simtimer.cpp
	${SRCDIR}/game/shared/singleplay_gamerules.cpp
	${SRCDIR}/game/shared/SoundEmitterSystem.cpp
	${SRCDIR}/game/shared/soundenvelope.cpp
	${SRCDIR}/public/SoundParametersInternal.cpp
	spatialentitymgr.cpp
	splinepatch.cpp
	${SRCDIR}/game/shared/Sprite.cpp
	spritemodel.cpp
	${SRCDIR}/game/shared/SpriteTrail.cpp
	${SRCDIR}/game/shared/steamworks_gamestats.cpp
	${SRCDIR}/game/shared/studio_shared.cpp
	${SRCDIR}/game/shared/takedamageinfo.cpp
	${SRCDIR}/game/shared/teamplay_gamerules.cpp
	${SRCDIR}/game/shared/teamplayroundbased_gamerules.cpp
	${SRCDIR}/game/shared/test_ehandle.cpp
	text_message.cpp
	texturescrollmaterialproxy.cpp
	timematerialproxy.cpp
	toggletextureproxy.cpp
	${SRCDIR}/game/shared/usercmd.cpp

	${SRCDIR}/game/shared/usermessages.cpp
	${SRCDIR}/game/shared/util_shared.cpp
	${SRCDIR}/game/shared/vehicle_viewblend_shared.cpp
	vgui_avatarimage.cpp
	vgui_basepanel.cpp
	vgui_bitmapbutton.cpp
	vgui_bitmapimage.cpp
	vgui_bitmappanel.cpp
	vgui_centerstringpanel.cpp
	vgui_consolepanel.cpp
	vgui_debugoverlaypanel.cpp
	vgui_fpspanel.cpp
	vgui_game_viewport.cpp
	vgui_grid.cpp
	vgui_int.cpp
	vgui_loadingdiscpanel.cpp
	vgui_messagechars.cpp
	vgui_netgraphpanel.cpp
	vgui_slideshow_display_screen.cpp
	view.cpp
	view_beams.cpp
	view_effects.cpp
	view_scene.cpp
	viewangleanim.cpp
	ViewConeImage.cpp
	viewdebug.cpp
	viewpostprocess.cpp
	viewrender.cpp
	${SRCDIR}/game/shared/voice_banmgr.cpp
	${SRCDIR}/game/shared/voice_status.cpp
	vscript_client.cpp
	${SRCDIR}/game/shared/vscript_shared.cpp
	warp_overlay.cpp
	WaterLODMaterialProxy.cpp
	${SRCDIR}/game/shared/weapon_parse.cpp
	weapon_selection.cpp
	weapons_resource.cpp
	WorldDimsProxy.cpp
	vgui_hudvideo.cpp
	vgui_video.cpp
	${SRCDIR}/game/shared/mp_shareddefs.cpp

	c_basetempentity.cpp
	c_effects.cpp
	c_impact_effects.cpp
	c_movie_explosion.cpp
	c_particle_fire.cpp
	c_particle_smokegrenade.cpp
	c_prop_vehicle.cpp
	c_recipientfilter.cpp
	c_smoke_trail.cpp
	c_smokestack.cpp
	c_steamjet.cpp
	c_stickybolt.cpp
	c_te.cpp
	c_te_armorricochet.cpp
	c_te_basebeam.cpp
	c_te_beamentpoint.cpp
	c_te_beaments.cpp
	c_te_beamfollow.cpp
	c_te_beamlaser.cpp
	c_te_beampoints.cpp
	c_te_beamring.cpp
	c_te_beamringpoint.cpp
	c_te_beamspline.cpp
	c_te_bloodsprite.cpp
	c_te_bloodstream.cpp
	c_te_breakmodel.cpp
	c_te_bspdecal.cpp
	c_te_bubbles.cpp
	c_te_bubbletrail.cpp
	c_te_clientprojectile.cpp
	c_te_decal.cpp
	c_te_dynamiclight.cpp
	c_te_effect_dispatch.cpp
	c_te_energysplash.cpp
	c_te_explosion.cpp
	c_te_fizz.cpp
	c_te_footprint.cpp
	c_te_glassshatter.cpp
	c_te_glowsprite.cpp
	c_te_impact.cpp
	c_te_killplayerattachments.cpp
	c_te_largefunnel.cpp
	c_te_legacytempents.cpp
	c_te_muzzleflash.cpp
	c_te_particlesystem.cpp
	c_te_physicsprop.cpp
	c_te_playerdecal.cpp
	c_te_projecteddecal.cpp
	c_te_showline.cpp
	c_te_smoke.cpp
	c_te_sparks.cpp
	c_te_sprite.cpp
	c_te_spritespray.cpp
	c_te_worlddecal.cpp
	c_testtraceline.cpp
	c_tracer.cpp
	fx.cpp
	fx_discreetline.cpp
	fx_envelope.cpp
	fx_line.cpp
	fx_sparks.cpp
	particlesphererenderer.cpp
	smoke_fog_overlay.cpp

	game_controls/baseviewport.cpp
	game_controls/basemodel_panel.cpp
	game_controls/ClientScoreBoardDialog.cpp
	game_controls/commandmenu.cpp
	game_controls/intromenu.cpp
	game_controls/MapOverview.cpp
	game_controls/NavProgress.cpp
	game_controls/SpectatorGUI.cpp
	game_controls/teammenu.cpp
	game_controls/vguitextwindow.cpp
	game_controls/IconPanel.cpp

	mp3player.cpp

	${SRCDIR}/public/tools/bonelist.cpp
	entity_client_tools.cpp
	toolframework_client.cpp)
	
project_sources(
	${SRCDIR}/public/closedcaptions.cpp
	${SRCDIR}/public/posedebugger.cpp
	${SRCDIR}/public/client_class.cpp
	${SRCDIR}/common/compiledcaptionswap.cpp
	${SRCDIR}/public/collisionutils.cpp
	${SRCDIR}/public/crtmemdebug.cpp
	${SRCDIR}/public/dt_recv.cpp
	${SRCDIR}/public/dt_utlvector_common.cpp
	${SRCDIR}/public/dt_utlvector_recv.cpp
	${SRCDIR}/public/filesystem_helpers.cpp
	${SRCDIR}/public/interpolatortypes.cpp
	${SRCDIR}/public/networkvar.cpp
	${SRCDIR}/common/randoverride.cpp
	${SRCDIR}/public/rope_physics.cpp
	${SRCDIR}/public/scratchpad3d.cpp
	${SRCDIR}/public/ScratchPadUtils.cpp
	${SRCDIR}/public/sentence.cpp
	${SRCDIR}/game/shared/sheetsimulator.cpp
	${SRCDIR}/public/simple_physics.cpp
	${SRCDIR}/public/stringregistry.cpp
	${SRCDIR}/public/studio.cpp
	${SRCDIR}/public/vallocator.cpp
	${SRCDIR}/public/vgui_controls/vgui_controls.cpp
	${SRCDIR}/public/jigglebones.cpp
	${SRCDIR}/public/phonemeconverter.cpp
	hud_lcd.cpp
	in_mouse.cpp
	rendertexture.cpp
	${SRCDIR}/common/CegClientWrapper.cpp)

target_link_libraries(${PROJECT_NAME} ${SRCDIR}/lib/public/${PLATSUBDIR}/bitmap.lib)
target_link_libraries(${PROJECT_NAME} ${SRCDIR}/lib/public/${PLATSUBDIR}/bonesetup.lib)
target_link_libraries(${PROJECT_NAME} ${SRCDIR}/lib/public/${PLATSUBDIR}/choreoobjects.lib)
target_link_libraries(${PROJECT_NAME} ${SRCDIR}/lib/public/${PLATSUBDIR}/dmxloader.lib)
target_link_libraries(${PROJECT_NAME} ${SRCDIR}/lib/public/${PLATSUBDIR}/matsys_controls.lib)
target_link_libraries(${PROJECT_NAME} ${SRCDIR}/lib/public/${PLATSUBDIR}/mathlib.lib)
target_link_libraries(${PROJECT_NAME} ${SRCDIR}/lib/public/${PLATSUBDIR}/particles.lib)
target_link_libraries(${PROJECT_NAME} ${SRCDIR}/lib/public/${PLATSUBDIR}/raytrace.lib)
target_link_libraries(${PROJECT_NAME} ${SRCDIR}/lib/public/${PLATSUBDIR}/tier2.lib)
target_link_libraries(${PROJECT_NAME} ${SRCDIR}/lib/public/${PLATSUBDIR}/tier3.lib)
target_link_libraries(${PROJECT_NAME} ${SRCDIR}/lib/public/${PLATSUBDIR}/vgui_controls.lib)
target_link_libraries(${PROJECT_NAME} ${SRCDIR}/lib/public/${PLATSUBDIR}/videocfg.lib)
target_link_libraries(${PROJECT_NAME} ${SRCDIR}/lib/public/${PLATSUBDIR}/vtf.lib)
target_link_libraries(${PROJECT_NAME} ${SRCDIR}/lib/public/${PLATSUBDIR}/resourcefile.lib)
target_link_libraries(${PROJECT_NAME} ${SRCDIR}/lib/public/${PLATSUBDIR}/nvtc.lib)

if (NOT NO_STEAM)
	project_link_libraries(${STEAM_API_LIB})
endif()
