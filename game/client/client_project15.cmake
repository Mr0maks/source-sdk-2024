set(GAMENAME project15)

include(${SRCDIR}/game/client/client_base.cmake)

project_include_directories(project15 project15/VGUI ${SRCDIR}/game/shared/project15)
project_compile_definitions(CSTRIKE_DLL)

remove_project_sources(${SRCDIR}/game/shared/weapon_parse_default.cpp)

project_pch_sources(cbase.h
	project15/cs_replay.cpp

	c_mod_lesson_stubs.cpp
	c_team_objectiveresource.cpp
	hud_base_account.cpp
	hud_voicestatus.cpp
	hud_baseachievement_tracker.cpp
	${SRCDIR}/game/shared/predicted_viewmodel.cpp

	${SRCDIR}/game/shared/cs_achievements_and_stats_interface.cpp
	${SRCDIR}/game/shared/project15/achievements_cs.cpp
	${SRCDIR}/game/shared/project15/basecsgrenade_projectile.cpp
	project15/buy_presets/buy_preset.cpp
	project15/buy_presets/buy_preset_debug.cpp
	project15/buy_presets/buy_preset_weapon_info.cpp
	project15/buy_presets/buy_presets.cpp
	project15/c_cs_hostage.cpp
	project15/c_cs_player.cpp
	project15/c_cs_playerresource.cpp
	project15/c_cs_team.cpp
	project15/c_csrootpanel.cpp
	project15/c_plantedc4.cpp
	project15/c_te_radioicon.cpp
	project15/c_te_shotgun_shot.cpp
	project15/clientmode_csnormal.cpp
	${SRCDIR}/game/shared/project15/cs_ammodef.cpp
	${SRCDIR}/game/shared/project15/cs_gamemovement.cpp
	${SRCDIR}/game/shared/project15/cs_gamerules.cpp
	${SRCDIR}/game/shared/project15/cs_gamestats_shared.cpp
	project15/cs_in_main.cpp
	${SRCDIR}/game/shared/project15/cs_player_shared.cpp
	${SRCDIR}/game/shared/project15/cs_playeranimstate.cpp
	project15/cs_prediction.cpp
	${SRCDIR}/game/shared/project15/cs_shareddefs.cpp
	project15/cs_client_gamestats.cpp
	${SRCDIR}/game/shared/project15/cs_usermessages.cpp
	project15/cs_view_scene.cpp
	${SRCDIR}/game/shared/project15/cs_weapon_parse.cpp
	project15/fx_cs_blood.cpp
	project15/fx_cs_impacts.cpp
	project15/fx_cs_knifeslash.cpp
	project15/fx_cs_muzzleflash.cpp
	${SRCDIR}/game/shared/project15/fx_cs_shared.cpp
	project15/fx_cs_weaponfx.cpp
	${SRCDIR}/game/shared/project15/bot/shared_util.cpp
	project15/vgui_rootpanel_cs.cpp

	project15/cs_hud_ammo.cpp
	project15/cs_hud_chat.cpp
	project15/cs_hud_damageindicator.cpp
	project15/cs_hud_freezepanel.cpp
	project15/cs_hud_playerhealth.cpp
	project15/cs_hud_health.cpp
	project15/cs_hud_scope.cpp
	project15/cs_hud_target_id.cpp
	project15/cs_hud_weaponselection.cpp
	project15/hud_account.cpp
	project15/hud_armor.cpp
	project15/hud_c4.cpp
	project15/hud_deathnotice.cpp
	project15/hud_defuser.cpp
	project15/hud_flashbang.cpp
	project15/hud_hostagerescue.cpp
	project15/hud_progressbar.cpp
	project15/hud_radar.cpp
	project15/hud_roundtimer.cpp
	project15/hud_scenarioicon.cpp
	project15/hud_shopping_cart.cpp
	project15/cs_hud_achievement_announce.cpp
	project15/cs_hud_achievement_tracker.cpp
	project15/radio_status.cpp

	${SRCDIR}/game/shared/project15/weapon_ak47.cpp
	${SRCDIR}/game/shared/project15/weapon_aug.cpp
	${SRCDIR}/game/shared/project15/weapon_awp.cpp
	${SRCDIR}/game/shared/project15/weapon_basecsgrenade.cpp
	${SRCDIR}/game/shared/project15/weapon_c4.cpp
	${SRCDIR}/game/shared/project15/weapon_csbase.cpp
	${SRCDIR}/game/shared/project15/weapon_csbasegun.cpp
	${SRCDIR}/game/shared/project15/weapon_deagle.cpp
	${SRCDIR}/game/shared/project15/weapon_elite.cpp
	${SRCDIR}/game/shared/project15/weapon_famas.cpp
	${SRCDIR}/game/shared/project15/weapon_fiveseven.cpp
	${SRCDIR}/game/shared/project15/weapon_flashbang.cpp
	${SRCDIR}/game/shared/project15/weapon_g3sg1.cpp
	${SRCDIR}/game/shared/project15/weapon_galil.cpp
	${SRCDIR}/game/shared/project15/weapon_glock.cpp
	${SRCDIR}/game/shared/project15/weapon_hegrenade.cpp
	${SRCDIR}/game/shared/project15/weapon_knife.cpp
	${SRCDIR}/game/shared/project15/weapon_m249.cpp
	${SRCDIR}/game/shared/project15/weapon_m3.cpp
	${SRCDIR}/game/shared/project15/weapon_m4a1.cpp
	${SRCDIR}/game/shared/project15/weapon_mac10.cpp
	${SRCDIR}/game/shared/project15/weapon_mp5navy.cpp
	${SRCDIR}/game/shared/project15/weapon_p228.cpp
	${SRCDIR}/game/shared/project15/weapon_p90.cpp
	${SRCDIR}/game/shared/project15/weapon_scout.cpp
	${SRCDIR}/game/shared/project15/weapon_sg550.cpp
	${SRCDIR}/game/shared/project15/weapon_sg552.cpp
	${SRCDIR}/game/shared/project15/weapon_smokegrenade.cpp
	${SRCDIR}/game/shared/project15/weapon_tmp.cpp
	${SRCDIR}/game/shared/project15/weapon_ump45.cpp
	${SRCDIR}/game/shared/project15/weapon_usp.cpp
	${SRCDIR}/game/shared/project15/weapon_xm1014.cpp

	project15/VGUI/achievement_stats_summary.cpp
	project15/VGUI/achievements_page.cpp
	project15/VGUI/stats_summary.cpp
	project15/VGUI/stat_card.cpp
	project15/VGUI/base_stats_page.cpp
	project15/VGUI/match_stats_page.cpp
	project15/VGUI/lifetime_stats_page.cpp
	project15/VGUI/bordered_panel.cpp
	project15/VGUI/backgroundpanel.cpp
	project15/VGUI/buypreset_imageinfo.cpp
	project15/VGUI/buypreset_listbox.cpp
	project15/VGUI/buypreset_panel.cpp
	project15/VGUI/career_box.cpp
	project15/VGUI/career_button.cpp
	project15/VGUI/counterstrikeviewport.cpp
	project15/VGUI/cstrikebuyequipmenu.cpp
	project15/VGUI/cstrikebuymenu.cpp
	project15/VGUI/cstrikeclassmenu.cpp
	project15/VGUI/cstrikeclientscoreboard.cpp
	project15/VGUI/cstrikespectatorgui.cpp
	project15/VGUI/cstriketeammenu.cpp
	project15/VGUI/cstriketextwindow.cpp
	project15/vgui_c4panel.cpp
	project15/vgui_viewc4panel.cpp
	project15/VGUI/win_panel_round.cpp

	game_controls/buymenu.cpp
	game_controls/buysubmenu.cpp
	game_controls/classmenu.cpp

	# Project15
	project15/effects/c_chicken.cpp
	project15/effects/c_chicken.h
)

project_link_libraries(vtf)
