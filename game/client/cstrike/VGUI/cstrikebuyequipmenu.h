//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
// $NoKeywords: $
//=============================================================================//

#ifndef CSTRIKEBUYEQUIPMENU_H
#define CSTRIKEBUYEQUIPMENU_H
#ifdef _WIN32
#pragma once
#endif

#include <vgui_controls/WizardPanel.h>

#include <buymenu.h>
#include "backgroundpanel.h"

namespace vgui
{
	class Panel;
}

//============================
// CT Equipment menu
//============================
class CCSBuyEquipMenu_CT : public CCSBackgroundHaver<CBuyMenu>
{
private:
	typedef CCSBackgroundHaver<CBuyMenu> BaseClass;

public:
	CCSBuyEquipMenu_CT(IViewPort *pViewPort);

	virtual const char *GetName( void ) { return PANEL_BUY_EQUIP_CT; }
};


//============================
// Terrorist Equipment menu
//============================

class CCSBuyEquipMenu_TER : public CCSBackgroundHaver<CBuyMenu>
{
private:
	typedef CCSBackgroundHaver<CBuyMenu> BaseClass;

public:
	CCSBuyEquipMenu_TER(IViewPort *pViewPort);

	virtual const char *GetName( void ) { return PANEL_BUY_EQUIP_TER; }
};

#endif // CSTRIKEBUYEQUIPMENU_H
