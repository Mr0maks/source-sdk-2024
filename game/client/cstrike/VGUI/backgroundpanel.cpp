//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
// $NoKeywords: $
//=============================================================================//

#include "cbase.h"
#include "backgroundpanel.h"

#include <vgui/IVGui.h>
#include <vgui/IScheme.h>
#include <vgui/ISurface.h>
#include <vgui_controls/Label.h>
#include <vgui/ILocalize.h>
#include "vgui_controls/BuildGroup.h"
#include "vgui_controls/BitmapImagePanel.h"

using namespace vgui;

//-----------------------------------------------------------------------------
const int NumSegments = 7;
static int coord[NumSegments+1] = {
	0,
	1,
	2,
	3,
	4,
	6,
	9,
	10
};

//-----------------------------------------------------------------------------
void DrawRoundedBackground( Color bgColor, int wide, int tall )
{
	int x1, x2, y1, y2;
	surface()->DrawSetColor(bgColor);
	surface()->DrawSetTextColor(bgColor);

	int i;

	// top-left corner --------------------------------------------------------
	int xDir = 1;
	int yDir = -1;
	int xIndex = 0;
	int yIndex = NumSegments - 1;
	int xMult = 1;
	int yMult = 1;
	int x = 0;
	int y = 0;
	for ( i=0; i<NumSegments; ++i )
	{
		x1 = MIN( x + coord[xIndex]*xMult, x + coord[xIndex+1]*xMult );
		x2 = MAX( x + coord[xIndex]*xMult, x + coord[xIndex+1]*xMult );
		y1 = MAX( y + coord[yIndex]*yMult, y + coord[yIndex+1]*yMult );
		y2 = y + coord[NumSegments];
		surface()->DrawFilledRect( x1, y1, x2, y2 );

		xIndex += xDir;
		yIndex += yDir;
	}

	// top-right corner -------------------------------------------------------
	xDir = 1;
	yDir = -1;
	xIndex = 0;
	yIndex = NumSegments - 1;
	x = wide;
	y = 0;
	xMult = -1;
	yMult = 1;
	for ( i=0; i<NumSegments; ++i )
	{
		x1 = MIN( x + coord[xIndex]*xMult, x + coord[xIndex+1]*xMult );
		x2 = MAX( x + coord[xIndex]*xMult, x + coord[xIndex+1]*xMult );
		y1 = MAX( y + coord[yIndex]*yMult, y + coord[yIndex+1]*yMult );
		y2 = y + coord[NumSegments];
		surface()->DrawFilledRect( x1, y1, x2, y2 );
		xIndex += xDir;
		yIndex += yDir;
	}

	// bottom-right corner ----------------------------------------------------
	xDir = 1;
	yDir = -1;
	xIndex = 0;
	yIndex = NumSegments - 1;
	x = wide;
	y = tall;
	xMult = -1;
	yMult = -1;
	for ( i=0; i<NumSegments; ++i )
	{
		x1 = MIN( x + coord[xIndex]*xMult, x + coord[xIndex+1]*xMult );
		x2 = MAX( x + coord[xIndex]*xMult, x + coord[xIndex+1]*xMult );
		y1 = y - coord[NumSegments];
		y2 = MIN( y + coord[yIndex]*yMult, y + coord[yIndex+1]*yMult );
		surface()->DrawFilledRect( x1, y1, x2, y2 );
		xIndex += xDir;
		yIndex += yDir;
	}

	// bottom-left corner -----------------------------------------------------
	xDir = 1;
	yDir = -1;
	xIndex = 0;
	yIndex = NumSegments - 1;
	x = 0;
	y = tall;
	xMult = 1;
	yMult = -1;
	for ( i=0; i<NumSegments; ++i )
	{
		x1 = MIN( x + coord[xIndex]*xMult, x + coord[xIndex+1]*xMult );
		x2 = MAX( x + coord[xIndex]*xMult, x + coord[xIndex+1]*xMult );
		y1 = y - coord[NumSegments];
		y2 = MIN( y + coord[yIndex]*yMult, y + coord[yIndex+1]*yMult );
		surface()->DrawFilledRect( x1, y1, x2, y2 );
		xIndex += xDir;
		yIndex += yDir;
	}

	// paint between top left and bottom left ---------------------------------
	x1 = 0;
	x2 = coord[NumSegments];
	y1 = coord[NumSegments];
	y2 = tall - coord[NumSegments];
	surface()->DrawFilledRect( x1, y1, x2, y2 );

	// paint between left and right -------------------------------------------
	x1 = coord[NumSegments];
	x2 = wide - coord[NumSegments];
	y1 = 0;
	y2 = tall;
	surface()->DrawFilledRect( x1, y1, x2, y2 );
	
	// paint between top right and bottom right -------------------------------
	x1 = wide - coord[NumSegments];
	x2 = wide;
	y1 = coord[NumSegments];
	y2 = tall - coord[NumSegments];
	surface()->DrawFilledRect( x1, y1, x2, y2 );
}

//-----------------------------------------------------------------------------
void DrawRoundedBorder( Color borderColor, int wide, int tall )
{
	int x1, x2, y1, y2;
	surface()->DrawSetColor(borderColor);
	surface()->DrawSetTextColor(borderColor);

	int i;

	// top-left corner --------------------------------------------------------
	int xDir = 1;
	int yDir = -1;
	int xIndex = 0;
	int yIndex = NumSegments - 1;
	int xMult = 1;
	int yMult = 1;
	int x = 0;
	int y = 0;
	for ( i=0; i<NumSegments; ++i )
	{
		x1 = MIN( x + coord[xIndex]*xMult, x + coord[xIndex+1]*xMult );
		x2 = MAX( x + coord[xIndex]*xMult, x + coord[xIndex+1]*xMult );
		y1 = MIN( y + coord[yIndex]*yMult, y + coord[yIndex+1]*yMult );
		y2 = MAX( y + coord[yIndex]*yMult, y + coord[yIndex+1]*yMult );
		surface()->DrawFilledRect( x1, y1, x2, y2 );

		xIndex += xDir;
		yIndex += yDir;
	}

	// top-right corner -------------------------------------------------------
	xDir = 1;
	yDir = -1;
	xIndex = 0;
	yIndex = NumSegments - 1;
	x = wide;
	y = 0;
	xMult = -1;
	yMult = 1;
	for ( i=0; i<NumSegments; ++i )
	{
		x1 = MIN( x + coord[xIndex]*xMult, x + coord[xIndex+1]*xMult );
		x2 = MAX( x + coord[xIndex]*xMult, x + coord[xIndex+1]*xMult );
		y1 = MIN( y + coord[yIndex]*yMult, y + coord[yIndex+1]*yMult );
		y2 = MAX( y + coord[yIndex]*yMult, y + coord[yIndex+1]*yMult );
		surface()->DrawFilledRect( x1, y1, x2, y2 );
		xIndex += xDir;
		yIndex += yDir;
	}

	// bottom-right corner ----------------------------------------------------
	xDir = 1;
	yDir = -1;
	xIndex = 0;
	yIndex = NumSegments - 1;
	x = wide;
	y = tall;
	xMult = -1;
	yMult = -1;
	for ( i=0; i<NumSegments; ++i )
	{
		x1 = MIN( x + coord[xIndex]*xMult, x + coord[xIndex+1]*xMult );
		x2 = MAX( x + coord[xIndex]*xMult, x + coord[xIndex+1]*xMult );
		y1 = MIN( y + coord[yIndex]*yMult, y + coord[yIndex+1]*yMult );
		y2 = MAX( y + coord[yIndex]*yMult, y + coord[yIndex+1]*yMult );
		surface()->DrawFilledRect( x1, y1, x2, y2 );
		xIndex += xDir;
		yIndex += yDir;
	}

	// bottom-left corner -----------------------------------------------------
	xDir = 1;
	yDir = -1;
	xIndex = 0;
	yIndex = NumSegments - 1;
	x = 0;
	y = tall;
	xMult = 1;
	yMult = -1;
	for ( i=0; i<NumSegments; ++i )
	{
		x1 = MIN( x + coord[xIndex]*xMult, x + coord[xIndex+1]*xMult );
		x2 = MAX( x + coord[xIndex]*xMult, x + coord[xIndex+1]*xMult );
		y1 = MIN( y + coord[yIndex]*yMult, y + coord[yIndex+1]*yMult );
		y2 = MAX( y + coord[yIndex]*yMult, y + coord[yIndex+1]*yMult );
		surface()->DrawFilledRect( x1, y1, x2, y2 );
		xIndex += xDir;
		yIndex += yDir;
	}

	// top --------------------------------------------------------------------
	x1 = coord[NumSegments];
	x2 = wide - coord[NumSegments];
	y1 = 0;
	y2 = 1;
	surface()->DrawFilledRect( x1, y1, x2, y2 );

	// bottom -----------------------------------------------------------------
	x1 = coord[NumSegments];
	x2 = wide - coord[NumSegments];
	y1 = tall - 1;
	y2 = tall;
	surface()->DrawFilledRect( x1, y1, x2, y2 );

	// left -------------------------------------------------------------------
	x1 = 0;
	x2 = 1;
	y1 = coord[NumSegments];
	y2 = tall - coord[NumSegments];
	surface()->DrawFilledRect( x1, y1, x2, y2 );

	// right ------------------------------------------------------------------
	x1 = wide - 1;
	x2 = wide;
	y1 = coord[NumSegments];
	y2 = tall - coord[NumSegments];
	surface()->DrawFilledRect( x1, y1, x2, y2 );
}

//-----------------------------------------------------------------------------
// Purpose: transform a normalized value into one that is scaled based the minimum
//          of the horizontal and vertical ratios
//-----------------------------------------------------------------------------
static int GetAlternateProportionalValueFromNormal(int normalizedValue)
{
	int wide, tall;
	GetHudSize( wide, tall );
	int proH, proW;
	surface()->GetProportionalBase( proW, proH );
	double scaleH = (double)tall / (double)proH;
	double scaleW = (double)wide / (double)proW;
	double scale = (scaleW < scaleH) ? scaleW : scaleH;

	return (int)( normalizedValue * scale );
}

//-----------------------------------------------------------------------------
// Purpose: transform a standard scaled value into one that is scaled based the minimum
//          of the horizontal and vertical ratios
//-----------------------------------------------------------------------------
int GetAlternateProportionalValueFromScaled( HScheme hScheme, int scaledValue)
{
	return GetAlternateProportionalValueFromNormal( scheme()->GetProportionalNormalizedValueEx( hScheme,  scaledValue ) );
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
CCSBackground::CCSBackground()
	: m_BgColor(0,0,0,0)
	, m_FgColor(255,255,255,255)
{
	// corners --------------------------------------------
	m_pTopLeftPanel = scheme()->GetImage( "gfx/vgui/round_corner_nw", false );
	m_pTopRightPanel = scheme()->GetImage( "gfx/vgui/round_corner_ne", false );
	m_pBottomLeftPanel = scheme()->GetImage( "gfx/vgui/round_corner_sw", false );
	m_pBottomRightPanel = scheme()->GetImage( "gfx/vgui/round_corner_se", false );
	
	// background -----------------------------------------
	m_pTopSolid = scheme()->GetImage( "gfx/vgui/solid_background", false );
	m_pUpperMiddleSolid = scheme()->GetImage( "gfx/vgui/solid_background", false );
	m_pLowerMiddleSolid = scheme()->GetImage( "gfx/vgui/solid_background", false );
	m_pBottomSolid = scheme()->GetImage( "gfx/vgui/solid_background", false );
	
	// transparent border ---------------------------------
	m_pTopClear = scheme()->GetImage( "gfx/vgui/trans_background", false );
	m_pBottomClear = scheme()->GetImage( "gfx/vgui/trans_background", false );
	m_pLeftClear = scheme()->GetImage( "gfx/vgui/trans_background", false );
	m_pRightClear = scheme()->GetImage( "gfx/vgui/trans_background", false );
	
	// Logo -----------------------------------------------
	m_pExclamationPanel = scheme()->GetImage( "gfx/vgui/CS_logo", false );
}

//-----------------------------------------------------------------------------
// Purpose: Sets colors etc for background image panels
//-----------------------------------------------------------------------------
void CCSBackground::ApplyBackgroundSchemeSettings( vgui::IScheme *pScheme )
{
	m_BgColor = Color( 255, 255, 255, pScheme->GetColor( "BgColor", Color( 0, 0, 0, 0 ) )[3] );
	m_FgColor = pScheme->GetColor( "FgColor", Color( 0, 0, 0, 0 ) );
}

static void DrawImage( IImage *image, int left, int top, int wide, int tall )
{
	surface()->DrawSetTexture( image->GetID() );
	surface()->DrawTexturedRect( left, top, left + wide, top + tall );
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void CCSBackground::PaintBackground( vgui::EditablePanel *pWindow )
{
	int screenWide, screenTall;
	pWindow->GetSize( screenWide, screenTall );

	int inset = GetAlternateProportionalValueFromNormal( 20 );
	int cornerSize = GetAlternateProportionalValueFromNormal( 10 );

	int titleHeight = GetAlternateProportionalValueFromNormal( 42 );
	int mainHeight = GetAlternateProportionalValueFromNormal( 376 );

	int logoSize = titleHeight;

	vgui::surface()->DrawSetColor( m_BgColor );

	// corners --------------------------------------------
	DrawImage( m_pTopLeftPanel, inset, inset, cornerSize, cornerSize );
	DrawImage( m_pTopRightPanel, screenWide - inset - cornerSize, inset, cornerSize, cornerSize );
	DrawImage( m_pBottomLeftPanel, inset, screenTall - inset - cornerSize, cornerSize, cornerSize );
	DrawImage( m_pBottomRightPanel, screenWide - inset - cornerSize, screenTall - inset - cornerSize, cornerSize, cornerSize );

	// background -----------------------------------------
	DrawImage( m_pTopSolid, inset + cornerSize, inset, screenWide - 2*inset - 2*cornerSize, cornerSize );
	DrawImage( m_pUpperMiddleSolid, inset, inset + cornerSize, screenWide - 2*inset, titleHeight );
	DrawImage( m_pLowerMiddleSolid, inset + cornerSize, screenTall - inset - cornerSize, screenWide - 2*inset - 2*cornerSize, cornerSize );
	DrawImage( m_pBottomSolid, inset, screenTall - inset - cornerSize - mainHeight, screenWide - 2*inset, mainHeight );

	// transparent border ---------------------------------
	DrawImage( m_pTopClear, 0, 0, screenWide, inset );
	DrawImage( m_pBottomClear, 0, screenTall - inset, screenWide, inset );
	DrawImage( m_pLeftClear, 0, inset, inset, screenTall - 2*inset );
	DrawImage( m_pRightClear, screenWide - inset, inset, inset, screenTall - 2*inset );

	vgui::surface()->DrawSetColor( m_FgColor );

	// Logo -----------------------------------------------
	int logoInset = (cornerSize + titleHeight - logoSize)/2;
	DrawImage( m_pExclamationPanel, inset + logoInset, inset + logoInset, logoSize, logoSize );
}

//-----------------------------------------------------------------------------
// Purpose: Resizes windows to fit completely on-screen (for 1280x1024), and
//          centers them on the screen.  Sub-controls are also resized and moved.
//-----------------------------------------------------------------------------
void CCSBackground::PerformBackgroundLayout( vgui::EditablePanel *pWindow )
{
	// center window xpos
	int x, y;
	pWindow->GetPos( x, y );

	int wide, tall;
	pWindow->GetSize( wide, tall );

	int screenWide, screenTall;
	GetHudSize(screenWide, screenTall );

	x = ( screenWide - wide ) / 2;

	pWindow->SetPos( x, y );
}
