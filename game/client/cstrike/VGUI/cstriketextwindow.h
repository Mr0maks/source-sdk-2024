//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
// $NoKeywords: $
//=============================================================================//

#ifndef CSTEXTWINDOW_H
#define CSTEXTWINDOW_H
#ifdef _WIN32
#pragma once
#endif

#include "vguitextwindow.h"
#include "backgroundpanel.h"

//-----------------------------------------------------------------------------
// Purpose: displays the MOTD
//-----------------------------------------------------------------------------

class CCSTextWindow : public CCSBackgroundHaver<CTextWindow>
{
private:
	typedef CCSBackgroundHaver<CTextWindow> BaseClass;

public:
	CCSTextWindow(IViewPort *pViewPort);
	virtual ~CCSTextWindow();

	virtual void Update();
	virtual void SetVisible(bool state);
	virtual void ShowPanel( bool bShow );
	virtual void OnKeyCodePressed(vgui::KeyCode code);

protected:
	ButtonCode_t m_iScoreBoardKey;
};


#endif // CSTEXTWINDOW_H
