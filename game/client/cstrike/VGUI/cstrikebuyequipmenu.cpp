//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
// $NoKeywords: $
//=============================================================================//

#include "cbase.h"
#include "cstrikebuyequipmenu.h"
#include "cs_shareddefs.h"
#include "cstrikebuysubmenu.h"
#include "backgroundpanel.h"

using namespace vgui;

//-----------------------------------------------------------------------------
// Purpose: Constructor for CT Equipment menu
//-----------------------------------------------------------------------------
CCSBuyEquipMenu_CT::CCSBuyEquipMenu_CT(IViewPort *pViewPort) : CCSBackgroundHaver<CBuyMenu>( pViewPort )
{
	SetTitle( "#Cstrike_Buy_Menu", true);

	SetProportional( true );

	m_pMainMenu = new CCSBuySubMenu( this, "BuySubMenu" );
	m_pMainMenu->LoadControlSettings( "Resource/UI/BuyEquipment_CT.res" );
	m_pMainMenu->SetVisible( false );

	m_iTeam = TEAM_CT;
}

//-----------------------------------------------------------------------------
// Purpose: Constructor for Terrorist Equipment menu
//-----------------------------------------------------------------------------
CCSBuyEquipMenu_TER::CCSBuyEquipMenu_TER(IViewPort *pViewPort) : CCSBackgroundHaver<CBuyMenu>( pViewPort )
{
	SetTitle( "#Cstrike_Buy_Menu", true);

	SetProportional( true );

	m_pMainMenu = new CCSBuySubMenu( this, "BuySubMenu" );
	m_pMainMenu->LoadControlSettings( "Resource/UI/BuyEquipment_TER.res" );
	m_pMainMenu->SetVisible( false );

	m_iTeam = TEAM_TERRORIST;
}
