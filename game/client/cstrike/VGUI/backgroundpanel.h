//========= Copyright Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
// $NoKeywords: $
//=============================================================================//

#ifndef CSBACKGROUND_H
#define CSBACKGROUND_H

#include <vgui_controls/Frame.h>
#include <vgui_controls/EditablePanel.h>

class IViewPort;

//-----------------------------------------------------------------------------
// Purpose: transform a standard scaled value into one that is scaled based the minimum
//          of the horizontal and vertical ratios
//-----------------------------------------------------------------------------
int GetAlternateProportionalValueFromScaled( vgui::HScheme scheme, int scaledValue );

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void DrawRoundedBackground( Color bgColor, int wide, int tall );

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void DrawRoundedBorder( Color borderColor, int wide, int tall );

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
class CCSBackground
{
public:
	CCSBackground();

	void ApplyBackgroundSchemeSettings(vgui::IScheme *pScheme);
	void PaintBackground(vgui::EditablePanel *pWindow);
	void PerformBackgroundLayout(vgui::EditablePanel *pWindow);

private:
	Color m_BgColor;
	Color m_FgColor;
	
	vgui::IImage *m_pTopLeftPanel;
	vgui::IImage *m_pTopRightPanel;
	vgui::IImage *m_pBottomLeftPanel;
	vgui::IImage *m_pBottomRightPanel;
	
	vgui::IImage *m_pTopSolid;
	vgui::IImage *m_pUpperMiddleSolid;
	vgui::IImage *m_pLowerMiddleSolid;
	vgui::IImage *m_pBottomSolid;
	
	vgui::IImage *m_pTopClear;
	vgui::IImage *m_pBottomClear;
	vgui::IImage *m_pLeftClear;
	vgui::IImage *m_pRightClear;
	
	vgui::IImage *m_pExclamationPanel;
};

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
template<class T>
class CCSBackgroundHaver : public T, public CCSBackground
{
public:
	CCSBackgroundHaver(IViewPort *pViewPort)
		: T(pViewPort)
	{
	}

	CCSBackgroundHaver(IViewPort *pViewPort, const char *panelName)
		: T(pViewPort, panelName)
	{
	}

	CCSBackgroundHaver(vgui::Panel *parent, const char *name)
		: T(parent, name)
	{
	}

	void ApplySchemeSettings(vgui::IScheme *pScheme) OVERRIDE
	{
		T::ApplySchemeSettings(pScheme);
		CCSBackground::ApplyBackgroundSchemeSettings(pScheme);
	}

	void PaintBackground() OVERRIDE
	{
		CCSBackground::PaintBackground(this);
	}

	void PerformLayout() OVERRIDE
	{
		T::PerformLayout();
		CCSBackground::PerformBackgroundLayout(this);
	}

	void ShowPanel(bool bShow) // don't override
	{
		if (bShow)
		{
			// this is done to make sure background is
			// properly layouted before drawing (TODO: chache???)
			this->InvalidateLayout(false, false);
		}

		T::ShowPanel(bShow);
	}
};

#endif // CSBACKGROUND_H
