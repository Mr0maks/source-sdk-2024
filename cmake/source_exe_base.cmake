add_executable(${PROJECT_NAME} WIN32)

# default output directory for executables
if (NOT OUTBINDIR)
	set(OUTBINDIR ${OUTDIR})
endif()

# common stuff
include(${SRCDIR}/cmake/source_base.cmake)
