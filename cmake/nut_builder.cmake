if (WINDOWS)
	set(PERL ${SRCDIR}/devtools/srcsrv/perl/bin/perl.exe)
else()
	set(PERL perl)
endif()

function(add_nut FILENAME)
	set(BASENAME)
		
	get_filename_component(BASENAME ${FILENAME} NAME_WLE)
	
	add_custom_command(
		OUTPUT ${CMAKE_CURRENT_BINARY_DIR}/${BASENAME}_nut.h
		COMMAND ${PERL}
		ARGS ${SRCDIR}/devtools/bin/texttoarray.pl ${FILENAME} g_Script_${BASENAME} > ${CMAKE_CURRENT_BINARY_DIR}/${BASENAME}_nut.h
		COMMENT "${BASENAME}.nut produces ${BASENAME}_nut.h"
		VERBATIM)
		
	project_sources(${CMAKE_CURRENT_BINARY_DIR}/${BASENAME}_nut.h)
endfunction()
