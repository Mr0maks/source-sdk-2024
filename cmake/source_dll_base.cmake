add_library(${PROJECT_NAME} SHARED)

# no libvstdlib, please
set_target_properties(${PROJECT_NAME} PROPERTIES PREFIX "")

# default output directory for libraries
if (NOT OUTBINDIR)
	set(OUTBINDIR ${OUTDIR}/bin/${PLATSUBDIR})
endif()

# common stuff
include(${SRCDIR}/cmake/source_base.cmake)

# dll specific stuff
project_sources(${SRCDIR}/public/tier0/memoverride.cpp)

project_link_directories(${SRCDIR}/lib/${PLATSUBDIR})

target_link_libraries(${PROJECT_NAME} ${SRCDIR}/lib/public/${PLATSUBDIR}/interfaces.lib)
target_link_libraries(${PROJECT_NAME} ${SRCDIR}/lib/public/${PLATSUBDIR}/tier0.lib)
target_link_libraries(${PROJECT_NAME} ${SRCDIR}/lib/public/${PLATSUBDIR}/tier1.lib)
target_link_libraries(${PROJECT_NAME} ${SRCDIR}/lib/public/${PLATSUBDIR}/vstdlib.lib)