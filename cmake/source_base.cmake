#------------------------------------------------------------------------------
# compiler configuration
#------------------------------------------------------------------------------

if (MSVC)
	project_compile_definitions(COMPILER_MSVC)

	if (CMAKE_SIZEOF_VOID_P EQUAL 8)
		project_compile_definitions(COMPILER_MSVC64)
	else()
		project_compile_definitions(COMPILER_MSVC32)
	endif()

	# disable narrowing conversion warnings
	project_compile_options(/wd4838)

else()
	# gcc (what else could it be)
	project_compile_definitions(COMPILER_GCC)
endif()

#------------------------------------------------------------------------------
# platform configuration
#------------------------------------------------------------------------------

if (PLATFORM_64BITS)
	project_compile_definitions(PLATFORM_64BITS)
endif()

if (WIN64)
	project_compile_definitions(WIN64)
endif()

if (POSIX)
	# TODO: remove _POSIX (already thought it was unused but then the single place where it's used fucked me over)
	project_compile_definitions(POSIX _POSIX)
endif()

project_compile_definitions(_DLL_EXT=${CMAKE_SHARED_LIBRARY_SUFFIX} PLATSUBDIR="${PLATSUBDIR}")
	
if (LINUX)
	# TODO: remove _LINUX
	project_compile_definitions(_LINUX LINUX)

	# mac does not want this
	project_link_options(
		-Wl,--wrap=fopen
		-Wl,--wrap=freopen
		-Wl,--wrap=open
		-Wl,--wrap=creat
		-Wl,--wrap=access
		-Wl,--wrap=__xstat
		-Wl,--wrap=stat
		-Wl,--wrap=lstat
		-Wl,--wrap=fopen64
		-Wl,--wrap=open64
		-Wl,--wrap=opendir
		-Wl,--wrap=__lxstat
		-Wl,--wrap=chmod
		-Wl,--wrap=chown
		-Wl,--wrap=lchown
		-Wl,--wrap=symlink
		-Wl,--wrap=link
		-Wl,--wrap=__lxstat64
		-Wl,--wrap=mknod
		-Wl,--wrap=utimes
		-Wl,--wrap=unlink
		-Wl,--wrap=rename
		-Wl,--wrap=utime
		-Wl,--wrap=__xstat64
		-Wl,--wrap=mount
		-Wl,--wrap=mkfifo
		-Wl,--wrap=mkdir
		-Wl,--wrap=rmdir
		-Wl,--wrap=scandir
		-Wl,--wrap=realpath)

	# this is what valve does for 64-bit linux builds
	# we could bump this up, the svencoop machine should be a good baseline
	if (PLATFORM_64BITS)
		project_compile_options(-march=nocona)
	else()
		project_compile_options(-march=pentium4)
	endif()
elseif (OSXALL)
	project_compile_definitions(_OSX OSX _DARWIN_UNLIMITED_SELECT FD_SETSIZE=10240 PLATFORM_OSX)

	# this is what valve does for 64-bit linux builds
	# we could bump this up, the svencoop machine should be a good baseline
	if (PLATFORM_64BITS)
		project_compile_options(-march=nocona)
	else()
		project_compile_options(-march=pentium4)
	endif()
elseif (FREEBSD)
	project_compile_definitions(PLATFORM_BSD PLATFORM_FREEBSD)
	# TODO(Er2): Change this?
	project_compile_options(-march=native)
endif()

#------------------------------------------------------------------------------
# OpenGL and SDL2 setup (yucky)
#------------------------------------------------------------------------------

if (GL)
	project_compile_definitions(GL_GLEXT_PROTOTYPES DX_TO_GL_ABSTRACTION)
else()
	project_compile_definitions(USE_ACTUAL_DX)
endif()

if (SDL)
	project_compile_definitions(USE_SDL)

	# add sdl to includes for all projects because this is what valve did
	project_include_directories(${SDL2_INCLUDE_DIRS})
endif()

if (WINDOWS)
	project_compile_definitions(AVI_VIDEO)
endif()

#------------------------------------------------------------------------------
# Other stuff
#------------------------------------------------------------------------------

# this has to be defined even when not building csgo (using CSGO_DLL for actually csgo specific stuff now)
project_compile_definitions(CSTRIKE15)

if (DEDICATED)
	project_compile_definitions(DEDICATED)
endif()

if (NO_STEAM)
	project_compile_definitions(NO_STEAM)
endif()

#------------------------------------------------------------------------------
# project configuration
#------------------------------------------------------------------------------

# include dirs that are needed by almost every project
project_include_directories(
	${SRCDIR}/common
	${SRCDIR}/public
	${SRCDIR}/public/tier0
	${SRCDIR}/public/tier1)
	
#------------------------------------------------------------------------------
# post build
#------------------------------------------------------------------------------

if (OUTDIR AND OUTBINDIR)
	add_custom_command(TARGET ${PROJECT_NAME} POST_BUILD
		COMMAND ${CMAKE_COMMAND} -E copy
		$<TARGET_FILE:${PROJECT_NAME}>
		${OUTBINDIR}/$<TARGET_FILE_NAME:${PROJECT_NAME}>)
endif()
