add_executable(${PROJECT_NAME})

# default output directory for console executables
if (NOT OUTBINDIR)
	set(OUTBINDIR ${OUTDIR}/bin/${PLATSUBDIR})
endif()

# common stuff
include(${SRCDIR}/cmake/source_base.cmake)

project_sources(${SRCDIR}/public/tier0/memoverride.cpp)

project_link_directories(${SRCDIR}/lib/${PLATSUBDIR})

# see helper_macros.cmake
if (USE_LINK_GROUPS)
	target_link_libraries(${PROJECT_NAME} -Wl,--start-group -Wl,--end-group)
endif()

project_link_libraries(interfaces tier0 tier1 vstdlib)
