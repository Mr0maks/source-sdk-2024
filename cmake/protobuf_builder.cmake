project_include_directories(${GENERATED_PROTO_DIR})

project_include_directories(${SRCDIR}/thirdparty/protobuf-2.5.0/src)

if(WINDOWS)
	set(PROTOC ${SRCDIR}/devtools/bin/protoc.exe)
elseif(OSXALL)
	set(PROTOC ${SRCDIR}/devtools/bin/osx64/protoc)
elseif(LINUX)
	set(PROTOC ${SRCDIR}/devtools/bin/linux/protoc)
elseif(FREEBSD)
	set(PROTOC ${SRCDIR}/devtools/bin/fbsd64/protoc)
else()
	message(FATAL_ERROR "Port me!")
endif()

function(add_protobuf FILENAME)
    set(BASENAME)
	set(NAME)
    get_filename_component(BASENAME ${FILENAME} NAME_WLE)
    get_filename_component(NAME ${FILENAME} NAME)

	add_custom_command(TARGET ${PROJECT_NAME} PRE_BUILD COMMAND ${CMAKE_COMMAND} -E make_directory ${GENERATED_PROTO_DIR})

    add_custom_command(
		OUTPUT ${GENERATED_PROTO_DIR}/${BASENAME}.pb.cc ${GENERATED_PROTO_DIR}/${BASENAME}.pb.h
		COMMAND ${PROTOC}
		ARGS --proto_path=${SRCDIR}/thirdparty/protobuf-2.5.0/src
		--proto_path=${SRCDIR}/game/shared/${GAMENAME}
		--proto_path=${SRCDIR}/game/shared
		--proto_path=${SRCDIR}/common
		--cpp_out=${GENERATED_PROTO_DIR} ${FILENAME}
		COMMENT "Running Protocol Buffer Compiler on ${NAME}..."
		VERBATIM)

    project_sources(${GENERATED_PROTO_DIR}/${BASENAME}.pb.cc)
endfunction()
