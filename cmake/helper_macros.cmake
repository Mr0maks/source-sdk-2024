# vpc migration helper macros

if (NOT MSVC AND NOT MACOS)
	set(USE_LINK_GROUPS ON)
endif()

macro(project_sources)
	target_sources(${PROJECT_NAME} PRIVATE ${ARGV})
endmacro()

macro(project_compile_options)
	target_compile_options(${PROJECT_NAME} PRIVATE ${ARGV})
endmacro()

macro(project_compile_definitions)
	target_compile_definitions(${PROJECT_NAME} PRIVATE ${ARGV})
endmacro()

macro(project_include_directories)
	target_include_directories(${PROJECT_NAME} PRIVATE ${ARGV})
endmacro()

macro(project_link_directories)
	target_link_directories(${PROJECT_NAME} PRIVATE ${ARGV})
endmacro()

macro(project_link_libraries)
	if (USE_LINK_GROUPS)
		# last entry should be -Wl,--end-group
		get_target_property(LIBS ${PROJECT_NAME} LINK_LIBRARIES)

		list(LENGTH LIBS LIST_LENGTH)
		math(EXPR LIST_LENGTH "${LIST_LENGTH}-1")

		list(INSERT LIBS ${LIST_LENGTH} ${ARGV})
		set_property(TARGET ${PROJECT_NAME} PROPERTY LINK_LIBRARIES ${LIBS})
	else()
		target_link_libraries(${PROJECT_NAME} ${ARGV})
	endif()
endmacro()

macro(project_link_options)
	target_link_options(${PROJECT_NAME} PRIVATE ${ARGV})
endmacro()

# removal macros are only used by tier0
macro(remove_project_sources)
	get_target_property(TARGET_SOURCES ${PROJECT_NAME} SOURCES)
	list(REMOVE_ITEM TARGET_SOURCES ${ARGV})
	set_property(TARGET ${PROJECT_NAME} PROPERTY SOURCES ${TARGET_SOURCES})
endmacro()
	
macro(remove_project_link_libraries)
	get_target_property(TARGET_LIBRARIES ${PROJECT_NAME} LINK_LIBRARIES)
	list(REMOVE_ITEM TARGET_LIBRARIES ${ARGV})
	set_property(TARGET ${PROJECT_NAME} PROPERTY LINK_LIBRARIES ${TARGET_LIBRARIES})
endmacro()

# pch stuff
if (MSVC)
	function(add_pch PCH_HEADER PCH_SOURCE)
		target_sources(${PROJECT_NAME} PRIVATE ${PCH_SOURCE})
		set_source_files_properties(${PCH_SOURCE} PROPERTIES COMPILE_FLAGS "/Yc${PCH_HEADER}")
	endfunction()
	
	function(project_pch_sources PCH_HEADER)
		set(SRC_FILES ${ARGN})		
		target_sources(${PROJECT_NAME} PRIVATE ${SRC_FILES})
		foreach(SRC ${SRC_FILES})
			set_source_files_properties(${SRC} PROPERTIES COMPILE_FLAGS "/Yu${PCH_HEADER}")			
		endforeach()
	endfunction()
else()
	function(add_pch PCH_HEADER PCH_SOURCE)
		target_sources(${PROJECT_NAME} PRIVATE ${PCH_SOURCE})
	endfunction()
	
	function(project_pch_sources PCH_HEADER)	
		set(SRC_FILES ${ARGN})
		target_sources(${PROJECT_NAME} PRIVATE ${SRC_FILES})
	endfunction()
endif()
