@echo off
setlocal

set TTEXE=..\..\devtools\bin\timeprecise.exe
if not exist %TTEXE% goto no_ttexe
goto no_ttexe_end

:no_ttexe
set TTEXE=time /t
:no_ttexe_end

rem echo.
rem echo ~~~~~~ buildallshaders %* ~~~~~~
%TTEXE% -cur-Q
set tt_all_start=%ERRORLEVEL%
set tt_all_chkpt=%tt_start%

set sourcedir="shaders"
set targetdir="..\..\..\game\platform\shaders"

set BUILD_SHADER=call buildshadersMODEL20b.bat

set ARG_EXTRA=

REM ****************
REM usage: buildallshaders [-pc ]
REM ****************
set ALLSHADERS_CONFIG=pc
if /i "%1" == "-pc" goto shcfg_pc

REM // The default is to build shaders for all platforms when not specifying -pc, -x360, or -ps3
call buildallshadersMODEL20b.bat -pc %*
goto end_of_file

:shcfg_pc
	echo.
	echo // PC (-pc command line option) ===============================================
	if /i "%2" == "-nompi" set ARG_EXTRA=-nompi
	set ALLSHADERS_CONFIG=pc
	goto shcfg_end
:shcfg_end

REM ****************
REM PC SHADERS
REM ****************
if /i "%ALLSHADERS_CONFIG%" == "pc" (
  %BUILD_SHADER% stdshader_dx9_20b_new %ARG_EXTRA%
)

REM ****************
REM END
REM ****************
:end

rem echo.
if not "%dynamic_shaders%" == "1" (
  rem echo Finished full buildallshaders %*
) else (
  rem echo Finished dynamic buildallshaders %*
)

rem %TTEXE% -diff %tt_all_start% -cur
rem echo.

:end_of_file
