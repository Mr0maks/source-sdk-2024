project(stdshader_dbg)

include(${SRCDIR}/cmake/source_dll_base.cmake)

project_include_directories(.. fxctmp9 vshtmp9)
project_compile_definitions(STDSHADER_DBG_DLL_EXPORT FAST_MATERIALVAR_ACCESS)

project_sources(
	BaseVSShader.cpp
	debugdepth.cpp
	DebugDrawEnvmapMask.cpp
	debugluxel.cpp
	debugnormalmap.cpp
	debugtangentspace.cpp
	fillrate.cpp)

if (WINDOWS)
	project_sources(../shader_dll_verify.cpp)
endif()

project_link_libraries(mathlib shaderlib)
